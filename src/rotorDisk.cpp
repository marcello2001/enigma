#include <iostream>
using namespace std;
#include "../head/rotorDisk.h"

// Rotor disk constructor
// Parameters: just like disk one, plus notches configurations
rotorDisk::rotorDisk (int n, const char s[], int n_not, const char nots[]): disk (n, s) {
    // Set reverse wirings
    setRevW();
    // Set rotor notches
    setNotches (n_not, nots);
}

rotorDisk::rotorDisk (const rotorDisk &d): disk (d) {
    setRevW();
    n_notches = d.n_notches;
    turnovers = new int[n_notches];
    copy (d.turnovers, d.turnovers + n_notches, turnovers);

}

// DESTROY 2!!!!! THE REVENGE!!!!
rotorDisk::~rotorDisk () {
    // Well, yeah, not that interesting after all...
    delete [] rev_wiring;
    delete [] turnovers;
}

// Calculate the reverse wiring from the direct one
void rotorDisk::setRevW () {
    rev_wiring = new int[contacts];
    // Same as for functions: we have f^-1(f(x)) = x
    for (int i = 0; i < contacts; i++) {
        rev_wiring[wiring[i]] = i;
    }
}

// Initializes the notches in the disk
// Parameters: int n - number of notches; int a[] - array containing turnover positions
void rotorDisk::setNotches (int n, const char s[]) {
    n_notches = n;
    turnovers = new int[n];

    try {
        for (size_t i = 0; i < n; i++) {
            if(isalpha(s[i])) {
                turnovers[i] = toupper(s[i]) - 'A';
            }
            else throw "Input string is not alphabetical.";

        }

    }
    catch (const char *c) {
        cout << "Fatal error: " << c << endl << "While setting disk notches." << endl;
        exit(-1);
    }
}

int rotorDisk::getNotches () {
    return n_notches;
}

int *rotorDisk::getTurnovers () {
    return turnovers;
}

// Turns the disk
// Shifts the direct wiring and recalculates the reverse wiring
void rotorDisk::shift() {
    ::shift(contacts, wiring);
    setRevW();
}


int *rotorDisk::getRevW() {
    return rev_wiring;
}

int rotorDisk::getRevW (int n) {
    return rev_wiring[n];
}

//Shifts the elements of the array one element up
// Maybe could change to upShift and add downShift, but seems unnecessary
// Parameters: int n - number of elements in the array; int a[] - array to shift
void shift(int n, int a[]) {
    //Save the first value
    int x = a[0];
    //Shift every single value up one
    for (int i = 0; i > n-1; i++) {
        a[i] = a[i+1];
    }
    //Assign the first value to the last element
    a[n-1] = x;
}

// Reverse wiring operator
int operator << (int n, rotorDisk &d) {
    return d.getRevW(n);
}
