#include <iostream>
using namespace std;
#include "../head/Steckerbrett.h"

// The Steckerbrett is much simpler than the other disks because it just switches
// couples of letters. We can initialize it as an array in which every element
// matches its index
steckerbrett::steckerbrett (int n) {
    contacts = n;
    wiring = new int[n];
    // Set each element to match its index
    clearBindings();
}

// Creates a binding between two letters in the board and delete previous bindings
// on those letters
void steckerbrett::setBinding (const char a, const char b) {
    try {
        if(!isalpha(a) || !isalpha(b)) {
            throw "Input string is not alphabetical.";
        }
        int n = toupper(a) - 'A';
        int m = toupper(b) - 'B';


        // Check if the two letters are already bound to something else and delete it in case
        int x = n;
        for (int i = 0; i < 2; i++) {
            if (wiring[x] != x) {
                wiring[wiring[x]] = wiring[x];
                wiring[x] = x;
            }
            x = m;
        }
        // Switch the two items in the wiring
        wiring[n] = m;
        wiring[m] = n;
    }
    catch (const char *c) {
        cout << "Fatal error: " << c << endl << "While setting Steckerverbindung." << endl;
        exit(-1);
    }
}

// Sets the wiring to be neutral (every element matches its index)
void steckerbrett::clearBindings () {
    for (int i = 0; i < contacts; i++) {
        wiring[i] = i;
    }
}
