#include <iostream>
#include <cctype>
using namespace std;
#include "../head/disk.h"

// Disk constructor
disk::disk (int n, const char s[]) {
    setWiring (n, s);
}

disk::disk (const disk &d) {
    contacts = d.contacts;
    wiring = new int[contacts];
    copy (d.wiring, d.wiring + contacts, wiring);
}


// DESTROY!!!
disk::~disk () {
    // Just get rid of him Muahahahahahah
    delete [] wiring;
}

void disk::setWiring (int n, const char s[]) {
    contacts = n;
    wiring = new int[n];

    try {
        for (size_t i = 0; i < n; i++) {
            if(isalpha(s[i])) {
                wiring[i] = toupper(s[i]) - 'A';
            }
            else throw "Input string is not alphabetical.";

        }

    }
    catch (const char *c) {
        cout << "Fatal error: " << c << endl << "While initializing disk wiring." << endl;
        exit(-1);
    }
}

int disk::getContacts () {
    return contacts;
}

int *disk::getWiring() {
    return wiring;
}

int disk::getWiring (int n) {
    return wiring[n];
}

// Disk coding operators
int operator >> (int n, disk &d) {
    return d.getWiring(n);
}
