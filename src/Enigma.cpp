// ENIGMA, the german jewel of cryptography
// By Marcello Fonda
// Started on May 20th 2016

// The program tries to simulate the mechanism of the well known german encoding machine,
// and to teach the author some C++ coding as well (I've been playing around C for a
// while and now I'm trying to understand a bit of object oriented programming).

// To know more about Enigma's working principles I recommend visiting Wikipedia
// or cryptomuseum.com, but it essentialy works as a series of Alberti Chiper
// Disks which successively encode every letter; the disks rotate at every keypress,
// providing a pseudo-irregular period to the message. Most of the Enigma machines
// are povided with a Steckerbrett, a plugboard which switches couples of letters
// before and after the encription.
// The strenght of the machine wasn't based on the complexity of the mechanism itself,
// but on the fact that there were ten million billion possible initial settings
// for the machine, making it very hard to find the right one.


#include <iostream>
using namespace std;

#include "../head/disk.h"
#include "../head/rotorDisk.h"
#include "../head/Steckerbrett.h"

int main(int argc, char const *argv[]) {

    disk Enigma_one_ukw_b (26, "EJMZALYXVBWFCRQUONTSPIKHGD");

    rotorDisk Enigma_one_I (26, "EKMFLGDQVZNTOWYHXUSPAIBRCJ", 1, "Q");

    steckerbrett Stecker (26);
    Stecker.setBinding ('A', 'G');
    Stecker.setBinding ('A', 'F');

    cout << "Willkommen, bitte schreiben Sie ein Buchstabe:" << endl << "> ";
    char i;
    cin >> i;
    i = toupper(i);

    char c = ((i-'A') >> Stecker >> Enigma_one_I >> Enigma_one_ukw_b << Enigma_one_I >> Stecker) + 'A';


    cout << c << endl;
    return 0;
}
