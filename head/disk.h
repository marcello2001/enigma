#ifndef DISK_INCLUDED
#define DISK_INCLUDED

// Just needed an empty array as default value so here is it. LOL
static int empty[] = {};

// Generic class, includes both rotor disks and reflectors
class disk {
protected:
    // Number of pins in the disk
    int contacts;
    // Coding patterns
    int *wiring;
public:
    // Constructors and destructor
    disk (int n = 0, const char s[] = "");
    disk (const disk &d);
    disk& operator= (const disk &d) {
        contacts = d.contacts;
        copy (d.wiring, d.wiring + contacts, wiring);
        return *this;
    }
    ~disk ();
    int getContacts();
    void setWiring (int n, const char s[]);
    int *getWiring ();
    int getWiring (int n);
};

// The coding operetoresss
int operator >> (int n, disk &d);

#endif /* end of include guard: DISK_INCLUDED */
