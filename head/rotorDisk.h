#ifndef ROTORDISK_INCLUDED
#define ROTORDISK_INCLUDED

#include "disk.h"

//Rotor disks can turn, and they have also a reverse encoding! Here they are
class rotorDisk: public disk {
private:
    // Reverse patterns; You could possibly use an unique pattern for both front and reverse, but this way seems simpler and time-saving
    int *rev_wiring;
    // Disks have notches to turn
    int n_notches;
    int *turnovers;
public:
    void setRevW ();
    // Constructor and destructor
    rotorDisk (int n = 0, const char s[] = "", int n_not = 0, const char nots[] = "");
    rotorDisk (const rotorDisk &d);

    rotorDisk& operator= (const rotorDisk &d) {
        contacts = d.contacts;
        copy (d.wiring, d.wiring + contacts, wiring);

        n_notches = d.n_notches;
        copy (d.turnovers, d.turnovers + n_notches, turnovers);

        return *this;
    }

    ~rotorDisk ();
    //Notches setup
    void setNotches (int n, const char s[]);
    int getNotches();
    int *getTurnovers();
    // Turn the disk
    void shift ();
    int *getRevW ();
    int getRevW (int n);
};

// You can shift the elements of an array, didn't you know? Well now you can!
void shift(int n, int a[]);

int operator << (int n, rotorDisk &d);

#endif /* end of include guard: ROTORDISK_INCLUDED */
