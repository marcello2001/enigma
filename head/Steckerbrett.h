#ifndef STECKERBRETT_INCLUDED
#define STECKERBRETT_INCLUDED

#include "disk.h"

class steckerbrett: public disk {
public:
    steckerbrett (int n);
    void setBinding(char a, char b);
    void clearBindings();
};

#endif /* end of include guard: STECKERBRETT_INCLUDED */
