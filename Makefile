all: bin/test03

bin/test03: Enigma.o disk.o rotorDisk.o Steckerbrett.o
	g++ Enigma.o disk.o rotorDisk.o Steckerbrett.o -o bin/test03

Enigma.o: src/Enigma.cpp head/disk.h head/rotorDisk.h head/Steckerbrett.h
	g++ -c src/Enigma.cpp

Steckerbrett.o: src/Steckerbrett.cpp head/Steckerbrett.h head/disk.h
	g++ -c src/Steckerbrett.cpp

rotorDisk.o: src/rotorDisk.cpp head/rotorDisk.h head/disk.h
	g++ -c src/rotorDisk.cpp

disk.o: src/disk.cpp head/disk.h
	g++ -c src/disk.cpp

clean:
	rm -f *.o bin/test02 *~ #*
